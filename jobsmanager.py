from subprocess import Popen, PIPE, STDOUT
import time
import timeit
import os
from os import path
from itertools import cycle

import logging
if len(logging.root.handlers) == 0:
    logging.basicConfig(level=logging.INFO)
logger = logging.getLogger('JobsManager')

SLURM_FILE = """#!/bin/bash
#SBATCH --job-name={name}
#SBATCH --output=slurm_%A_%a.out
#SBATCH --error=slurm_%A_%a.err
#SBATCH --partition={partition}
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --array=0-{njobs}{max_parallel}


date
hostname
pwd
echo "My SLURM_ARRAY_TASK_ID: " $SLURM_ARRAY_TASK_ID

cd {work_dir}
sh ./$SLURM_ARRAY_TASK_ID".sh"

date
"""

def is_slurm_finished(jobid):
    process = Popen("squeue -j {}".format(jobid), shell=True, stdout=PIPE, stderr=PIPE)
    out, _ = process.communicate()
    return out.decode().find(str(jobid)) == -1

def sec2time(seconds):
    m=seconds/60.0
    if m>=60.0:
        return "{:.2f}h".format(seconds/60.0/60.0)
    elif m>=1.0:
        return "{:.2f}min".format(m)
    else: return "{:.2f}sec".format(seconds)

class JobsManager():
    def __init__(self, *, jobs, work_dir, **kwargs):
        self.jobs = jobs
        self.work_dir = path.abspath(work_dir)

        logger.info("initializing JobsManager Object..")
        logger.info(f"work_dir:'{work_dir}'")
        logger.info(f"num_jobs: {len(jobs)}")

        assert not path.isdir(self.work_dir), f"work dir '{self.work_dir}' already exists"
        
        self.max_parallel = None
        self.partition = "core"

        for key, value in kwargs.items():
            logger.info(f"setting '{key}'='{value}'")
            setattr(self, key, value)

        if not hasattr(self, "name"):
            self.name = f"JobsManager_[{self.work_dir}]"

        self.jobid = []
        self.arrays_size = []

    def submit(self):
        njobs = len(self.jobs)
        if njobs == 0:
            logger.info("no jobs to run...")
            return self

        process = Popen("scontrol show conf | grep -i array", shell=True, stdout=PIPE, stderr=PIPE)
        out, err = process.communicate()
        max_slurm_array = [int(s) for s in out.split() if s.isdigit()][0]

        logger.info(f"maximum SLURM array size allowed is {max_slurm_array}")

        self.arrays_size=[max_slurm_array]*(njobs//max_slurm_array)
        remain = njobs % max_slurm_array
        if remain > 0 : self.arrays_size.append(remain)
        narrays = len(self.arrays_size)
        self.jobid=[-1]*narrays
        
        if narrays > 1: logger.info(f"array sizes: {self.arrays_size}")
        
        for i in range(narrays):
            logger.info(f"generating scripts for SLURM array...\n" \
                        f"in directory: {self.work_dir}\n" \
                        f"array {i+1}/{narrays} with {self.arrays_size[i]} jobs")

            output_dir=path.join(self.work_dir, "slurm_output")
            dir=path.join(self.work_dir, "slurm_temp")

            if narrays>1:
                output_dir+=str(i)
                dir+=str(i)

            os.makedirs(dir, exist_ok=False)
            os.makedirs(output_dir, exist_ok=False)

            for ind in range(self.arrays_size[i]):
                cmd=self.jobs[i*max_slurm_array+ind]
                with open(path.join(dir, "{}.sh".format(ind)), 'w') as file:
                    for line in cmd: file.write(str(line))

            maxp_str=""
            if self.max_parallel!=None:
                assert self.max_parallel>0
                maxp_str="%"+str(self.max_parallel)

            slurm_str = SLURM_FILE.format(
                name=self.name,
                partition=self.partition,
                njobs=self.arrays_size[i]-1,
                max_parallel=maxp_str,
                work_dir=dir)

            spath=path.join(output_dir, 'SLURM_ARRAY_SCRIPT.sh')
            with open(spath, 'w') as file:
                for line in slurm_str: file.write(line)

            logger.info("submitting array to SLURM...")

            slrm_cmd = f"cd {output_dir}; sbatch {spath};"

            process = Popen(slrm_cmd, shell=True, stdout=PIPE, stderr=PIPE)
            out, err = process.communicate()

            logger.info(out)
            if len(out) < 1:
                logger.error("error occured in submitting slurm")
                return self

            self.jobid[i] = [int(s) for s in out.split() if s.isdigit()][0]

            logger.info(f"Job id {i+1}/{narrays} is: {self.jobid[i]}")

        return self

    def wait(self):

        narrays=len(self.arrays_size)

        if narrays == 0:
            logger.info("no jobs to submit...")
            return self


        start = time.time()

        finished=False

        njobs_all =len(self.jobs)
        ncompleted_all_old = 0
        npending_all_old = njobs_all
        count=0

        while not finished:

            told=time.time()

            ncompleted = [0] * narrays
            nrunning = [0] * narrays
            npending = [0] * narrays

            finished = True

            for i in range(narrays):

                finished=finished and is_slurm_finished(self.jobid[i])

                process = Popen(
                    "squeue -r -t RUNNING -j {jobid} | wc -l; squeue -r -t PENDING -j {jobid} | wc -l;"
                        .format(jobid=self.jobid[i]), shell=True, stdout=PIPE, stderr=PIPE)
                out, err = process.communicate()
                nums = [max(int(s) - 1, 0) for s in out.split() if s.isdigit()]


                nrunning[i]=nums[0]
                npending[i]=nums[1]
                ncompleted[i] = self.arrays_size[i] - nrunning[i] - npending[i]

            ncompleted_all=sum(ncompleted)
            npending_all = sum(npending)

            if ncompleted_all - ncompleted_all_old > int(njobs_all / min(100, njobs_all)) or  npending_all_old-npending_all > int(njobs_all / min(100, njobs_all)) or count==0 or time.time()-told>60.0:
                ncompleted_all_old = ncompleted_all
                npending_all_old = npending_all

                if narrays > 1:
                    for i in range(narrays):
                        logger.info(f"array {i+1}/{narrays} id {self.jobid[i]}: "\
                                    f"Completed {ncompleted[i]}/{self.arrays_size[i]} ({ncompleted[i] * 100.0 / self.arrays_size[i]:.1f}%):" \
                                    f" running {nrunning[i]} pending {npending[i]}")

                logger.info(f"Completed {ncompleted_all}/{njobs_all} ({ncompleted_all * 100.0 / njobs_all:.1f}%): "\
                            f"Running {sum(nrunning)} "\
                            f"Pending {sum(npending)}... "\
                            f"{self.jobid} {sec2time(time.time() - start)}")

            count=count+1
            time.sleep(1.0)

        logger.info("FINISHED ALL SLURM JOBS")
        logger.info("Clearing auxilary JobManager SLURM directories....")
        for i in range(narrays):
            temp_dir=path.join(self.work_dir,"slurm_temp")
            if narrays>1: temp_dir+=str(i)
            logger.info(temp_dir)
            Popen('rm -rf '+temp_dir, shell=True).wait()

        return self

def run_jobs_in_shell(jobs,jobs_names=[], nprocs=3,time_sleep=0.1):

    if len(jobs) == 0:
        return

    if jobs_names==[]: jobs_names=[""]*len(jobs)

    run_time = timeit.default_timer()
    plist = list(range(nprocs))
    process_run = [0]*nprocs
    pr_status = [1]*nprocs
    pr_data = [None]*nprocs

    # last job index and name running on each processes
    pjob = [-1]*nprocs
    pjob_name = ['']*nprocs
    jobs_stat = [0]*len(jobs)
    ptime = [0]*len(jobs)

    # main parallel loop
    for (ind, [job, name]) in enumerate(zip(jobs,jobs_names)):

        # find an available process j, if not stay in the while loop
        j = -1
        inds = cycle(plist)
        while j < 0:
            k = next(inds)
            if k==0 and pjob[0]>0: time.sleep(time_sleep)

            if process_run[k] != 0:
                pr_status[k] = pr_data[k].poll()
            if pr_status[k] is not None:
                j = k
                if pjob[j] > 0:
                    jobs_stat[pjob[j]-1] = 1
                    ptime[j] = timeit.default_timer()-ptime[j]
                    logger.info(f"#finished job {pjob[j]} {pjob_name[j]} {ptime[j]:g}sec done {sum(jobs_stat)}/{len(jobs)} proc {j+1}/{nprocs}")
                pjob[j] = ind+1
                pjob_name[j] = name


        ptime[j] = timeit.default_timer()
        logger.info(f"starting job {pjob[j]}/{len(jobs)}: {pjob_name[j]} proc {j+1}/{nprocs}")

        process_run[j] = 1
        pr_data[j] = Popen(job, shell=True)

    # wait until open processes are finished
    flag = 0
    while not flag:
        flag = 1
        for j in plist:
            if process_run[j] > 0:
                pr_status[j] = pr_data[j].poll()
                if pr_status[j] is not None:
                    process_run[j] = -process_run[j]
                    jobs_stat[pjob[j]-1] = 1
                    ptime[j] = timeit.default_timer()-ptime[j]
                    logger.info(f"#finished job {pjob[j]} {pjob_name[j]} {ptime[j]:g}sec done {sum(jobs_stat)}/{len(jobs)} proc {j+1}/{nprocs}")

            flag = flag & (pr_status[j] is not None)
        time.sleep(time_sleep)

    run_time = timeit.default_timer()-run_time
    logger.info(f"finished parallel run {run_time} sec")

if __name__ == '__main__':
    njobs=85000
    work_dir = '/home/menahemk/check_jobs'
    jobs = [f"cd {work_dir}; echo '{i}' > {i}.txt;".format(i+1, i+1) for i in range(njobs)]
    JobsManager(jobs=jobs, work_dir=work_dir).submit().wait()

    # run_jobs_in_shell(jobs=jobs)